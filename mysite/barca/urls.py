from django.urls import path
from . import views

urlpatterns = [
    path('', views.barca_list, name='barca_list'),
    path('barca_history/', views.barca_history, name = 'barca_history'),
    path('barca_table/', views.barca_table, name = 'barca_table'),
    path('barca_season/', views.barca_season, name='barca_season'),
    path('create/', views.barca_create, name = 'barca_create'),
    path('<int:pk>/update/', views.barca_update, name='barca_update'),
    path('<int:pk>/delete',views.barca_delete, name='barca_delete'),
]