# Generated by Django 3.0.2 on 2020-01-18 12:48

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('barca', '0002_auto_20200114_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='barca',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата создании'),
        ),
        migrations.AddField(
            model_name='barca',
            name='published_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата публикации'),
        ),
    ]
