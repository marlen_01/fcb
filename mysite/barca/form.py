from django import forms
from .models import Barca


class BarcaForm(forms.ModelForm):

    class Meta:
        model = Barca
        fields = ('title', 'text', 'image')

