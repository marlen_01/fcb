from django.shortcuts import render, redirect, get_object_or_404
from .models import Barca, Timtable
from .form import BarcaForm


def barca_list(request):
    barca = Barca.objects.all()
    return render(request, 'barca/barca_list.html', {'barca':barca})

def barca_history(request):
    return render(request, 'barca/barca_history.html', {})

def barca_table(request):
    return render(request, 'barca/barca_table.html', {})       

def barca_season(request):
    return render(request, 'barca/barca_season.html', {})

def barca_create(request):
    if request.method == "POST":
        form = BarcaForm(request.POST, request.FILES)
        if form.is_valid():
            barca = form.save(commit=False)
            barca.save()
            return redirect('barca_list')
    else:
        form = BarcaForm()
    return render(request, 'barca/barca_create.html', {'form': form})

def barca_update(request, pk):
    barca = get_object_or_404(Barca, pk=pk)
    if request.method == "POST":
        form = BarcaForm(request.POST, request.FILES, instance=barca)
        if form.is_valid():
            barca = form.save(commit=False)
            barca.save()
            return redirect('barca_list')
    else:
        form =  BarcaForm(instance=barca)
    return render(request, 'barca/barca_update.html', {'form': form})

def barca_delete(request, pk):
    barca = get_object_or_404(Barca, pk=pk)
    barca.delete()
    return redirect('barca_list')
