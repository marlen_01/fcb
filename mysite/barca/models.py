from django.conf import settings
from django.db import models
from django.utils import timezone


class Barca(models.Model):
    title = models.CharField(max_length=200,verbose_name='Заголовок')
    text = models.TextField(verbose_name='Текст новости')
    image = models.ImageField(verbose_name='Изображение новости', upload_to='news_logo',blank=True, null=True)
    created_date = models.DateTimeField(verbose_name='Дата создании', default=timezone.now)
    published_date = models.DateTimeField(verbose_name='Дата публикации',  blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Timtable(models.Model):
    date = models.TextField (verbose_name='Дата')
    match = models.TextField(verbose_name='Матч')
    score = models.URLField(verbose_name='Счет')
    game = models.CharField(max_length=50, verbose_name='Соревнование')